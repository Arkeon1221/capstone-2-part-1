const User = require('../models/User')
const Product = require('../models/Product')
const bcrypt = require('bcrypt')
const auth = require('../auth')


module.exports.checkIfEmailExists = (data) => {
	return User.find({email: data.email}).then((result) => {
		if(result.length > 0){
			return true
		}

		return false
	})
}

module.exports.register = (data) => {
	let encrypted_password = bcrypt.hashSync(data.password, 10)

	let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email,
		mobileNumber: data.mobileNumber,
		password: encrypted_password
	})

	return new_user.save().then((created_user, error) => {
		if(error){
			return false 
		}

		return {
			message: 'User successfully registered!'
		}
	})
}


// Login User
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null){
			return {
				message: "User doesn't exist!"
			}
		}

		console.log(result)

		const is_password_correct = bcrypt.compareSync(data.password, result.password)
		// const greet_name = data.email

		if(is_password_correct) {
			return {
				message: `Login successful! Welcome ${result.firstName}!`,
				accessToken: auth.createAccessToken(result)
			}
		}

		return {
			message: 'Password is incorrect!'
		}

		
	})
}

// Get single user based on ID from token
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Makes the password not be included in the result
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});
};


// Non-admin User checkout (Create Order)

module.exports.checkout = async (data) => {

	if(data.isAdmin)
		return Promise.resolve({
				message: "You must be a user to checkout"
			})
	
		let is_user_updated = await User.findById(data.customerId).then((user) => {
			user.orders.push({
				productId: data.productId
			})

			return user.save().then((updated_user, error) => {
				if(error){
					return false
				}

				return true
			}) 
		})

		// Check if product is done adding the user to its customers array
		let is_product_updated = await Product.findById(data.productId).then((product) => {
			product.customers.push({
				customerId: data.customerId
			})

			return product.save().then((updated_product, error) => {
				if(error){
					return false
				}

				return true
			}) 
		})

		// Check if both the user and course have been updated successfully, and return a success message if so
		if(is_user_updated && is_product_updated){
			return {
				message: 'Purchase successful!'
			}
		}

		// If the enrollment failed, return 'Something went wrong.'
		return {
			message: 'Something went wrong.'
		}
	}


// Retrieve ALL users (ADMIN onlyy)

module.exports.retrieveAllUsers = (data) => {

	if(data.isAdmin){
		return User.find({}).then((result) => {
			return result
		})

	}

		let message = Promise.resolve({
		message: 'User must be ADMIN to access this!'
	})

	return message.then((value) => {
		return value
	})
	
}

// STRETCH GOAL ===========================================================================================================
// Set user as admin (Admin only)

module.exports.setAdmin = (data, user_Id) => {

	if(data.isAdmin){

		return User.findById(data.user_Id).then((result) => {

			if(result.isAdmin){

				return {

					message: `${result.firstName} is already an ADMIN!`}
			}

			return User.findByIdAndUpdate(user_Id, {
				isAdmin: true
				

			}).then((admin_set, error) => {
				if(error){
					return false
				}

				return {
					message: `${result.firstName} has been granted ADMIN role successfully!`
				}
			})
		})

		

	}	

			let message = Promise.resolve({
			message: 'User must be ADMIN to access this!'
		})

		return message.then((value) => {
			return value
		})
}


// Delete USER (ADMIN only)

module.exports.deleteUser = (data, user_Id) => {

	if(data.isAdmin){

		return User.findByIdAndDelete(user_Id).then((admin_set, error) => {
			if(error){
				return false
			}

			return {
				message:  'The user account has been granted ADMIN role successfully'
			}
		})

	}	

			let message = Promise.resolve({
			message: 'User must be ADMIN to access this!'
		})

		return message.then((value) => {
			return value
		})
}

// Retrieve All orders(ADMIN only)

// module.exports.getAllorders = (data, user_id) => {

// 	if(data.isAdmin){


// 		return User.findById(data, {password: 0}).then((result) => {
// 			return result 
// 		})

// 	}

// 		let message = Promise.resolve({
// 		message: 'User must be ADMIN to access this!'
// 	})

// 	return message.then((value) => {
// 		return value
// 	})

// }


// =======================================================================================



// module.exports.checkout = async (data) => {
// 	// console.log(data)
// 	// console.log(result)

// 	if(data.isAdmin)
// 		return Promise.resolve({
// 				message: "You must be a user to checkout"
// 			})
	
// 	let is_user_updated = await User.findById(data.userId).then((user) => {

// 		console.log(user)
// 		// console.log(user.orders.length)

// 		let i = user.orders.length

// 		let totalAmount = data.quantity * data.price
// 		console.log(totalAmount)

// 		user.orders.push({

// 			totalAmount: totalAmount

			
// 		})

		
// 		user.orders[i].products.push({
// 			productId: data.productId,
// 			productName: data.productName,
// 			brand: data.brand,
// 			price: data.price,
// 			quantity: data.quantity

// 		})

// 		return user.save().then((updated_user, error) => {
// 			if(error){
// 				return false
// 			}

// 			return true
// 		})
// 	})

// 	let is_product_updated = await Product.findById(data.productId).then((product) => {


// 		let custName = User.findById(data.userId).then((customer_details) => {

// 			let customer_name = data.firstName + data.lastName

// 			console.log(customer_name)

// 		})
		

// 		product.orders.push({
// 			customerId: data.userId
// 			// customerName: custName 
// 		})

// 		return product.save().then((updated_product, error) => {
// 			if(error){
// 				return false
// 			}

// 			return true
// 		})
// 	})
	
// 	if(is_user_updated && is_product_updated){
// 		return {
// 			message: 'User checkout is successful!'
// 		}
// 	}

// 	return {
// 		message: 'Something went wrong...'
// 	}
// }


