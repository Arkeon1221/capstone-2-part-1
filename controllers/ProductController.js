const Product = require('../models/Product')

// Create Product if ADMIN

// module.exports.createProduct = (data) => {
// 	if(data.isAdmin){
// 		let new_product = new Product({
// 			productName: data.productName,
// 			brand: data.brand,
// 			description: data.description,
// 			price: data.price,
// 			stocks: data.stocks
// 		})

// 		return new_product.save().then((new_product, error) => {
// 			if(error){
// 				return false
// 			}

// 			return {
// 				message: 'New product successfully added!'
// 			}
// 		})
// 	}

// 	let message = Promise.resolve({
// 		message: 'User must be ADMIN to access this!'
// 	})

// 	return message.then((value) => {
// 		return value
// 	})
// }





module.exports.createProduct = (data) => {

	let new_product = new Product({
			productName: data.productName,
			brand: data.brand,
			category: data.category,
			description: data.description,
			price: data.price,
			stocks: data.stocks
		})

	return new_product.save().then((new_product, error) => {
		if(error){
			return false 
		}

		return {
			message: 'Product successfully created!'
		}
	})
}





// module.exports.createProduct = (data) => {

// 	let new_product = new Product({
// 			productName: data.productName,
// 			brand: data.brand,
// 			description: data.description,
// 			price: data.price,
// 			stocks: data.stocks
// 		})

// 	return new_product.save().then((new_product, error) => {
// 		if(error){
// 			return false 
// 		}

// 		return {
// 			message: 'Product successfully created!'
// 		}
// 	})
// }








// Retrieve ALL Products
module.exports.retrieveAllProducts = () => {
	return Product.find({}).then((result) => {
		return result
	})
	console.log(result)
}


// Retrieve all ACTIVE products

module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then((result) => {
		return result
	})
}


// Retrieve all LEATHER shoes

module.exports.getLeather = () => {
	return Product.find({category: "Leather Shoes"}).then((result) => {
		return result
	})
}


// Retrieve all BAGS and Accessories

module.exports.getBag = () => {
	return Product.find({category: "Bags and Accessories"}).then((result) => {
		return result
	})
}

module.exports.getRubber = () => {
	return Product.find({category: "Rubber Shoes"}).then((result) => {
		return result
	})
}

// Retrieve SINGLE product
module.exports.retrieveProduct = (product_id) => {
	return Product.findById(product_id).then((result) => {
		return result
	})
}


// Update Product information (Admin only)

module.exports.updateProduct = (product_id, data) => {

	if(data.isAdmin) {

		return Product.findByIdAndUpdate(product_id, {
			productName: data.product.productName,
			brand: data.product.brand,
			category: data.product.category,
			
			description: data.product.description,
			stocks: data.product.stocks,
			price: data.product.price
		}).then((result, error) => {
			if(error){
				return false
			}

			return {
				message: 'Product has been updated successfully!'
			}
		})


	}

	let message = Promise.resolve({
		message: 'User must be ADMIN to access this!'
	})

	return message.then((value) => {
		return value
	})
	
}

// Archive Product (ADMIN)

module.exports.archiveProduct = (data, product_Id) => {

	if(data.isAdmin){

		return Product.findByIdAndUpdate(product_Id, {
			isActive: false
			
		}).then((archived_product, error) => {
			if(error){
				return false
			}

			return {
				message:  'The product has been archived successfully'
			}
		})

	}	

			let message = Promise.resolve({
			message: 'User must be ADMIN to access this!'
		})

		return message.then((value) => {
			return value
		})
}

// ACTIVATE Product (ADMIN)

module.exports.activateProduct = (data, product_Id) => {

	if(data.isAdmin){

		return Product.findByIdAndUpdate(product_Id, {
			isActive: true
			
		}).then((archived_product, error) => {
			if(error){
				return false
			}

			return {
				message:  'The product has been activated successfully'
			}
		})

	}	

			let message = Promise.resolve({
			message: 'User must be ADMIN to access this!'
		})

		return message.then((value) => {
			return value
		})
}




// ==================================================================================



