const User = require('../models/User')
const Product = require('../models/Product')
const Cart = require('../models/Cart')
const bcrypt = require('bcrypt')
const auth = require('../auth')





module.exports.AddToCart = async (data) => {

	if(data.isAdmin)
		return Promise.resolve({
				message: "You must be a user to checkout"
			})
	
		let is_user_updated = await User.findById(data.customerId).then((user) => {
			user.cart.push({
				productId: data.productId
			})

			return user.save().then((updated_user, error) => {
				if(error){
					return false
				}

				return true
			}) 
		})

		// Check if cart is done adding the user to its customers array
		let is_cart_updated = await Product.findById(data.productId).then((cart) => {
			cart.customers.push({
				customerId: data.customerId
			})

			return cart.save().then((updated_cart, error) => {
				if(error){
					return false
				}

				return true
			}) 
		})

		// Check if both the user and course have been updated successfully, and return a success message if so
		if(is_user_updated && is_product_updated){
			return {
				message: 'Product has been added to Cart!'
			}
		}

		// If the enrollment failed, return 'Something went wrong.'
		return {
			message: 'Something went wrong.'
		}
	}



module.exports.AddToCart = (data) => {

	let new_cart = new Cart({
			productName: data.productName,
			brand: data.brand,
			category: data.category,
			description: data.description,
			price: data.price,
			stocks: data.stocks
		})

	return new_cart.save().then((new_cart, error) => {
		if(error){
			return false 
		}

		return {
			message: 'Product successfully added to cart!'
		}
	})
}