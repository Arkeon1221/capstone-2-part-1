const mongoose = require('mongoose')

const product_schema = new mongoose.Schema({

	productName: {
		type: String,
		required: [true, 'Product name is required']
	},

	brand: {
		type: String,
		required: [true, 'Product brand is required!']
	},

	description: {
		type: String,
		required: [true, 'Description is required']
	},

	price: {
		type: Number,
		required: [true, 'Price is required!']
	},

	stocks: {
		type: Number,
		default: 0
	},

	category: {
		type: String,
		required: [true, 'Category is required']
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	customers: [{
	
		customerId: {
			type: String,
			required: [true, 'Order ID is required!']
		},

		customerName: {
			type: String,
			required: [false, 'customerName is required!']
		},

		purchasedOn: {
				type: Date,
				default: new Date()
			}

	}]

	
})

module.exports = mongoose.model('Product', product_schema)

