const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, 'First name is required.']
	},

	lastName: {
		type: String,
		required: [true, 'Last name is required.']
	},

	email: {
		type: String,
		required: [true, 'Email name is required.']
	},

	password: {
		type: String,
		required: [true, 'Password is required']
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	mobileNumber: {
		type: String, 
		required: [true, 'Mobile number is required.']
	},
	
	orders: [
		{
			productId: {
				type: String,
				required: [true, 'Product ID is required.']
			},

			purchasedOn: {
				type: Date,
				default: new Date()

			},

			quantity: {
				type: Number,
				default: 1
			},

			totalAmount: {
				type: Number,
				default: 1
			}

		}
	]
	

})
module.exports = mongoose.model('User', user_schema)