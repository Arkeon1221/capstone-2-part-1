const mongoose = require('mongoose')

const cart_schema = new mongoose.Schema({

	brand: {
		type: String,
		required: [true, 'Product brand is required!']
	},

	description: {
		type: String,
		required: [true, 'Description is required']
	},

	price: {
		type: Number,
		required: [true, 'Price is required!']
	},

	category: {
		type: String,
		required: [true, 'Category is required']
	},

	purchasedOn: {
				type: Date,
				default: new Date()
			
	},

	items: {
		type: Number,
		default: 0
	},

	orders: [
		{
			productId: {
				type: String,
				required: [true, 'Product ID is required.']
			},

			purchasedOn: {
				type: Date,
				default: new Date()

			},

			quantity: {
				type: Number,
				default: 1
			},

			totalAmount: {
				type: Number,
				default: 1
			}

		}
	]

	
})

module.exports = mongoose.model('Cart', cart_schema)

