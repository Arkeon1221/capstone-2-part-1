const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')

// Check if email exists
router.post("/check-email", (request, response) => {
	UserController.checkIfEmailExists(request.body).then((result) => {
		response.send(result)
	})
})

// User registration
router.post("/register", (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})

// Login user
router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})

// Get user details from token
router.get("/details", auth.verify, (request, response) => {

	// Retrieves the user data from the token
	const user_data = auth.decode(request.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	UserController.getProfile({userId : user_data.id}).then(result => response.send(result));

});



// Non-admin User checkout (Create Order)
router.post("/checkout", auth.verify, (request, response) => {
	let data = {
		
		productId: request.body.productId,
		customerId: request.body.userId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		// customerId: auth.decode(request.headers.authorization).id
	}

	UserController.checkout(data).then((result) => {
		response.send(result)

	})
})


// Retrieve single user details(ADMIN only for security)
router.get("/:id/details", auth.verify, (request, response) => {

	const data = {

		product_Id: request.params.productId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.getUserDetails(data, request.params.id).then((result) => {

		response.send(result)
	})
})



// Retrieve ALL users (ADMIN onlyy)

router.get("/retrieveAllUsers", auth.verify, (request, response) => {

	const data = {

		
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.retrieveAllUsers(data, request.params.id).then((result) => {

		response.send(result)
	})
})


// STRETCH GOAL ===========================================================================================================
// Set user as admin (Admin only)

router.patch('/:userId/setAdmin', auth.verify, (request,response) => {

	const data = {

		user_Id: request.params.userId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.setAdmin(data, request.params.userId).then((result) => {
		response.send(result)
	})
})


// Delete USER account (ADMIN only)

router.delete('/:userId/delete', auth.verify, (request,response) => {

	const data = {

		user_Id: request.params.userId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	UserController.deleteUser(data, request.params.userId).then((result) => {
		response.send(result)
	})
})
// ============================================================================================


module.exports = router