const express = require('express')
const router = express.Router()
const CartController = require('../controllers/CartController')
const auth = require('../auth')

// Add product to cart


router.post("/addtocart", auth.verify, (request, response) => {
	let data = {
		
		productId: request.body.productId,
		customerId: request.body.userId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		
	}

	CartController.AddToCart(data).then((result) => {
		response.send(result)

	})
})








module.exports = router