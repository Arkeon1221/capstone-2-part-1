const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')




router.post("/create", (request, response) => {
	ProductController.createProduct(request.body).then((result) => {
		response.send(result)
	})
})




// Retrieve ALL Products

router.get('/', (request, response) => {
	ProductController.retrieveAllProducts().then((result) => {
		response.send(result)
	})
})

// Retrieve all ACTIVE products

router.get('/activeProducts', (request, response) => {
	ProductController.getActiveProducts().then((result) => {
		response.send(result)
	})
})


// Get Leather shoes
router.get('/leather', (request, response) => {
	ProductController.getLeather().then((result) => {
		response.send(result)
	})
})

// Get rubber shoes
router.get('/rubber', (request, response) => {
	ProductController.getRubber().then((result) => {
		response.send(result)
	})
})

// Retrieve all BAGS and Accessories

router.get('/bags', (request, response) => {
	ProductController.getBag().then((result) => {
		response.send(result)
	})
})


// Retrieve SINGLE product
router.get('/:productId/retrieve', (request, response) => {
	ProductController.retrieveProduct(request.params.productId).then((result) => { 
		response.send(result)
	})
})

// Update Product information (Admin only)

router.patch('/:productId/update', auth.verify, (request, response) => {

	const data = {

			product: request.body,
			user: request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

	
	ProductController.updateProduct(request.params.productId, data).then((result) => {
		response.send(result)
	})
})



// Archive Product (ADMIN)

router.patch('/:productId/archive', auth.verify, (request,response) => {

	const data = {

		product_Id: request.params.productId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.archiveProduct(data, request.params.productId).then((result) => {
		response.send(result)
	})
})


// ACTIVATE Product (ADMIN)

router.patch('/:productId/activate', auth.verify, (request,response) => {

	const data = {

		product_Id: request.params.productId,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.activateProduct(data, request.params.productId).then((result) => {
		response.send(result)
	})
})

// =========================================================================================




module.exports = router