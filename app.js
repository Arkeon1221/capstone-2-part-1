const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const cartRoutes = require('./routes/cartRoutes')

dotenv.config()

const app = express()
const port = 8001



// MongoDB Connection
mongoose.connect(`mongodb+srv://Archeon1221:${process.env.MONGODB_PASSWORD}@cluster0.l6bvvoi.mongodb.net/Capstone-3-Booking-System?retryWrites=true&w=majority`, {
	


	useNewUrlParser: true,
	useUnifiedTopology: true 

})

let db = mongoose.connection
db.once('open', () => console.log('Connected to MongopDB'))
// MongoDB Connection END

// To avoid CORS errors when trying to send request to our server
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Routes
app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/carts', cartRoutes)
// Routes END

app.listen(process.env.PORT || 8001, () => {
	console.log(`API is now running on localhost: ${process.env.PORT || 8001}`)
})


// app.listen(port, () => {
// 	console.log(`API is now running on localhost: ${port}`)
// }) 